/* Copyright 2020 Joshua Mallad <zamnedix@zamnedix.net> */
/* This file is part of libisaac64, an implementation of Bob Jenkins'
   pseudo-random number generator. ISAAC64 is described at the following URL:
   https://burtleburtle.net/bob/rand/isaacafa.html 
   This file may be used under the terms of EITHER the MIT License 
   (see mit_license.txt) OR the GNU General Public License v3.0 
   (see gpl-3.0.txt). */
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/* Represents a single ISAAC64 pseudo-random number generator context */
struct isaac64 {
	uint64_t out[256];
	uint64_t buf[256];
	uint64_t a;
	uint64_t b;
	uint64_t c;
	uint16_t i;
};

/* Error codes returned from API calls */
enum isaac64_error {
	SUCCESS  =  0, /* No error */
	ENULLPTR = -1, /* Null pointer dereference detected */
	EALLOC   = -2  /* Dynamic allocation failure */
};

/****** Static functions ******/

/* An internal operation performed on an ISAAC64 context in two situations:
   1. During initialization: called by isaac64_init() or isaac64_seed() 
   immediately before they return.
   2. During a retrieval from the generator: called by isaac64_rand() if
   member `i' of the passed context is equal to 256. This function resets
   `i' to 0 before returning.
   
   Linus isn't big on people commenting the "how" rather than the "why" of
   code, but I've done the best I can here. My math skills aren't super strong,
   but I tried to port the following function as exactly as I could
   from GitHub user "skeeto"s Go implementation.
 */
static void isaac64_shuffle(struct isaac64* ctx)
{
	int i;
	uint64_t x;
	ctx->c++;
	ctx->b += ctx->c;
	for (i = 0; i < 256; i +=4) {
		x = ctx->buf[i];
		ctx->a ^= ctx->a ^ (ctx->a << 21);
		ctx->a += ctx->buf[(i+128)&0xff];
		ctx->buf[i] = ctx->buf[x>>3&0xff] + ctx->a + ctx->b;
		ctx->b = ctx->buf[ctx->buf[i]>>11&0xff] + x;
		ctx->out[i] = ctx->b;

		x = ctx->buf[i+1];
		ctx->a ^= ctx->a >> 5;
		ctx->a += ctx->buf[(i+1+128)&0xff];
		ctx->buf[i+1] = ctx->buf[x>>3&0xff] + ctx->a + ctx->b;
		ctx->b = ctx->buf[ctx->buf[i+1]>>11&0xff] + x;
		ctx->out[i+1] = ctx->b;

		x = ctx->buf[i+2];
		ctx->a ^= ctx->a << 12;
		ctx->a += ctx->buf[(i+2+128)&0xff];
		ctx->buf[i+2] = ctx->buf[x>>3&0xff] + ctx->a + ctx->b;
		ctx->b = ctx->buf[ctx->buf[i+2]>>11&0xff] + x;
		ctx->out[i+2] = ctx->b;

		x = ctx->buf[i+3];
		ctx->a ^= ctx->a >> 33;
		ctx->a += ctx->buf[(i+3+128)&0xff];
		ctx->buf[i+3] = ctx->buf[x>>3&0xff] + ctx->a + ctx->b;
		ctx->b = ctx->buf[ctx->buf[i+3]>>11&0xff] + x;
		ctx->out[i+3] = ctx->b;
	}
	ctx->i = 0;
}

/* Retrieve the next 64 bits from the given context. */
uint64_t isaac64_rand64(struct isaac64* ctx)
{
	uint64_t v;
	if (!ctx) {
		return 0;
	}
	if (ctx->i == 256) {
		isaac64_shuffle(ctx);
	}
	v = ctx->out[ctx->i];
	ctx->i++;
	return v;
}

/* Retrieve the next 32 bits from the given context. */
uint32_t isaac64_rand32(struct isaac64* ctx)
{
	return (isaac64_rand64(ctx) >> 32);
}

/* Retrieve the next 16 bits from the given context. */
uint16_t isaac64_rand16(struct isaac64* ctx)
{
	return (isaac64_rand64(ctx) >> 48);
}

/* Retrieve the next 8 bits from the given context. */
uint8_t isaac64_rand8(struct isaac64* ctx)
{
	return (isaac64_rand64(ctx) >> 56);
}

/****** Global functions ******/

/* Zero and free an ISAAC64 context */
void isaac64_free(struct isaac64* ctx)
{
	if (!ctx) {
		return;
	}
	memset(ctx, 0, sizeof(struct isaac64));
	free(ctx);
}

/* Allocate and zero a new ISAAC64 context */
int isaac64_new(struct isaac64** ctx)
{
	if (!ctx) {
		return ENULLPTR;
	}
	*ctx = malloc(sizeof(struct isaac64));
	if (!*ctx) {
		return EALLOC;
	}
	memset(*ctx, 0, sizeof(struct isaac64));
	return SUCCESS;
}

/* Zero an ISAAC64 context and initialize it from a supplied 2048-byte buffer
   of entropy */
int isaac64_init(struct isaac64* ctx, void* in)
{
	if (!ctx || !in) {
		return ENULLPTR;
	}
	memset(ctx, 0, sizeof(struct isaac64));
	memcpy(ctx->buf, in, 2048);
	isaac64_shuffle(ctx);
	return SUCCESS;
}

/* Zero an ISAAC64 context and initialize it from an 8-byte seed using
   the smaller SplitMix64 PRNG */
int isaac64_seed(struct isaac64* ctx, void* in)
{
	int i;
	uint64_t z;
	uint64_t s = 0;
	if (!ctx || !in) {
		return ENULLPTR;
	}
	memcpy(&s, in, 8);
	memset(ctx, 0, sizeof(struct isaac64));
	for (i = 0; i < 256; i++) {
		s += 0x9e3779b97f4a7c15;
		z = s;
		z ^= z >> 30;
		z *= 0xbf58476d1ce4e5b9;
		z ^= z >> 27;
		z *= 0x94d049bb133111eb;
		z ^= z >> 31;
		ctx->buf[i] = z;
	}
	isaac64_shuffle(ctx);
	return 0;
}

/* Fill the out buffer with sz bytes of random data from supplied context */
int isaac64_fill(struct isaac64* ctx, void* out, long sz)
{
	long i;
	long i8 = sz / 8; /* Number of 8-byte copies possible */
	long i1 = sz % 8; /* Number of 1-byte copies required */
	uint8_t* op = out;
	uint64_t v8 = 0;
	uint8_t v1 = 0;
	if (!ctx || !out) {
		return ENULLPTR;
	}
	if (!sz) {
		return SUCCESS;
	}
	for (i = 0; i < i8; i++) {
		v8 = isaac64_rand64(ctx);
		memcpy(op, &v8, 8);
		op += 8;
	}
	for (i = 0; i < i1; i++) {
		v1 = isaac64_rand64(ctx) >> 48;
		memcpy(op, &v1, 1);
		op += 1;
	}
	return SUCCESS;
}

/* Return a human-readable explanation of an error code */
const char* isaac64_strerror(int err)
{
	switch (err) {
	case SUCCESS:  return "Success";
	case ENULLPTR: return "Null pointer dereference";
	case EALLOC:   return "Dynamic allocation error";
	}
	return "Unknown";
}


# Copyright 2020 Joshua Mallad <zamnedix@zamnedix.net> */                      
# This file is part of libisaac64, an implementation of Bob Jenkins'           
# pseudo-random number generator. ISAAC64 is described at the following URL:   
# https://burtleburtle.net/bob/rand/isaacafa.html                              
# This file may be used under the terms of EITHER the MIT License              
# (see mit_license.txt) OR the GNU General Public License v3.0                 
# (see gpl-3.0.txt). 

# *********************** #
# Configuration variables #
# *********************** #

#C standard version to target 
STD := c89
# Location of gcc or clang compiler to use
CC := gcc

# ****************************************************************** #
# The following variables are NOT intended for general configuration #
# ****************************************************************** #

# Header include directories 
INCLUDES := ./include/public ./include/private
# Libraries to link against
LIBS := m
# Common source files for all targets
SRC := isaac64.c
# Final output path for shared library
LIB_OUT := libisaac64.so
# Directory of source files
SRC_DIR = src
# Directory for intermediate object output
OBJ_DIR := build/obj
# Directory for GCC dependency output
DEP_DIR := build/dep
#Compiler flags for default mode
CFLAGS_DEFAULT := -O2 -mtune=generic ${FEATURES_ENABLED} -ftrapv -fstack-protector-strong -fcf-protection=full -fstack-clash-protection -std=${STD}
#Compiler flags for fast mode	
CFLAGS_FAST := -O3 -g0 -march=native ${FEATURES_ENABLED} -std=${STD}
#Compiler flags for small mode
CFLAGS_SMALL := -Os -g0 -march=native ${FEATURES_ENABLED} -std=${STD}
#Compiler flags for static mode
CFLAGS_STATIC := -Os -g0 -static -Wl,-static ${FEATURES_ENABLED} -std=${STD}
#Compiler flags for debug mode
CFLAGS_DEBUG := -Og -ggdb -Wextra -Wall -pedantic -pedantic-errors -Werror \
	-Wno-switch -Wno-unused-parameter -Wno-unused-function -ftrapv \
	-fsanitize=leak -fsanitize=pointer-compare -fsanitize=pointer-subtract\
	-fsanitize=address -fsanitize-address-use-after-scope \
	-fsanitize=pointer-overflow -fsanitize=undefined \
	-fstack-protector-strong -fcf-protection=full \
	-fstack-clash-protection ${FEATURES_ENABLED} \
	-std=${STD}
#Compiler flag selection
ifneq (${fast},)
	CFLAGS := ${CFLAGS_FAST}
else ifneq (${small},)
	CFLAGS := ${CFLAGS_SMALL}
else ifneq (${static},)
	CFLAGS := ${CFLAGS_STATIC}
else ifneq (${dev},)
	CFLAGS := ${CFLAGS_DEBUG}
else
	CFLAGS := ${CFLAGS_DEFAULT}
endif
#Add header includes to CFLAGS
INCLUDES := ${foreach inc,${INCLUDES},-I${inc}}
CFLAGS := ${INCLUDES} ${CFLAGS}
#Common object files and dependencies for all targets
OBJ := ${foreach src,${SRC},${OBJ_DIR}/${patsubst %.c,%.o,${src}}}
DEP := $(patsubst %.c,%.d, $(SRC))
DEP := ${foreach dep,${DEP},${DEP_DIR}/${notdir ${dep}}}
#Linker flags
LDFLAGS := ${LDFLAGS} ${foreach lib,${LIBS},-l${lib}}

# ************** #
# Build targets  #
# ************** #

# This pseudo-target lists the names of all valid targets.
# .PHONY is not intended to be executed as a target. It serves to inform
# Make of the valid target names so that they do not clash with any files
# in the current working director.
.PHONY : all clean shared test

all: shared

shared: ${DEP} ${OBJ}
	@echo Linking shared library
	@${CC} ${CFLAGS} -shared -o ${LIB_OUT} ${OBJ} ${LDFLAGS}

test: ${DEP} ${OBJ}
	@echo Building test program
	@${CC} ${CFLAGS} -o test ${OBJ} src/isaac64_test.c

clean:
	rm -f ${DEP_DIR}/* ${OBJ_DIR}/* ${OUT}

-include ${DEP}

${DEP_DIR}/%.d: ${SRC_DIR}/%.c
	@echo Building dependencies for $<
	@${CC} ${CFLAGS} -MM -MF $@ -MT ${OBJ_DIR}/${patsubst %.c,%.o,${notdir $<}} -c $<

${OBJ_DIR}/%.o: ${SRC_DIR}/%.c
	@echo Compiling $<
	@${CC} ${CFLAGS} -c -fPIC $<
	@mv ${notdir $@} ${OBJ_DIR}




